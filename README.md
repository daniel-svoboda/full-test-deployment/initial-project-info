# Walk through a full Devops setup from scratch

# My little foreword to this project:
	- i am a big believer in actually showing something that works rather than 
	showing just a repo with info, therefore i have not only created distributed 
	repo with all setup from A to Z but also pushed it live to Gcloud for you to 
	actually see for yourself how it works and get a better feeling on how it all 
	comes together as a single piece. All backend API/simple Frontend App is working 
	all with auto-obtained https, reachable via single Public Static IP through 
    Kong Api Gateway.
	- what is completed: full stack DevOps setup including fully functional CICD 
	with Terraform+Helm setup of entire Google cloud with working Kubernetes cluster, 
	Letsencrypt TLS certificate autosetup, Kong API Gateway and working frontend that 
	talks to backend, which is working with Gcloud managed Postgres database all in 
	full HA mode (well all is prepared for it, but for free 300$ worth of credit i got 
	from Google i couldnt go all in on HA, backups etc.).
	- Full setup is live now, ready for you to try, play around and dig deeper around 
	the solution in Gcloud + Gitlab
	- btw please note on most important repos i followed branching strategy/git flow 
	process as well ('master' only for PROD and worked from 'dev')

# How-To:
[go here](https://gitlab.com/daniel-svoboda/full-test-deployment/initial-project-info/blob/master/docs/README.md)