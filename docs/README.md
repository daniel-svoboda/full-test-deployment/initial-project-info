# How to use this project:

# First
## Gitlab
- All project's repos are Public for anyone to access
  - the 10.000" view of all project repos can be found under:
    - https://gitlab.com/daniel-svoboda
  - Please start with following this Master repo:
    - https://gitlab.com/daniel-svoboda/full-test-deployment/setup-full-devops-from-scratch
      - it will lead you through all other repos one at a time

# Second
## Gcloud resources
- so that you can get full view + feel of the solution i have granted you read access to my gcloud project;s components (SQL, Kubernetes...)
- login to https://console.cloud.google.com
  - thegcloudtest@gmail.com
  - )9e(P%!My>a2{KAZ-VVgg2+T
    - for obvious reasons this user has READ-ONLY access and will be deleted right after you look at the solution
